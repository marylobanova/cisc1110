//Maria Lobanova, Laboratory 5

# include <iostream>
# include <iomanip>
# include <cmath>
# include <ctime>

using namespace std;

int main()
{
	char a,
		 b,
		 c;

	int num1,
		num2,
		userResult;

	unsigned seed;

	//use the time function to get a "seed" value for srand
	seed = time(0);
	srand(seed);

	//generate two random numbers in the range between 1 and 100
	num1 = rand() % 100 + 1;
	num2 = rand() % 100 + 1;

	//let user choose between addition (a) and subsctraction (b)
	cout << "Hello! This is a simple math tutoring program and you can practice addition or subsctraction with it.";
	cout << "\nTo practice addition, press enter 'a', to practice subsctraction enter 'b': ";
    cin >> c;

	//when user chose addition
	if (c == 'a') {
		cout << "You have chosen addition. Let's start the practice! " << endl << setw(6) << num1 << endl;
		cout << setw(4) << "+" <<num2 << endl << setw(6) << "------" << endl;
		cout << "\nWhat is the answer for the problem above? ";
		cin >> userResult;

		//compare user's result with actual result and print congratulations if answer is correct
		if (userResult == (num1 + num2))
			cout << "Your answer is correct!" << endl;

		//print correct answer
		else
			cout << "Your answer is not correct! Correct answer is " << num1 + num2 << ". Please try again. " << endl;
	}

	//when user chose subscrtraction
	else if (c == 'b') {
		cout << "You have chosen subscrtraction. Let's start the practice! " << endl << setw(6) << num1 << endl;
		cout << setw(4) << "-" <<num2 << endl << setw(6) << "------" << endl;
		cout << "\nWhat is the answer for the problem above? ";
		cin >> userResult;

		//compare user's result with actual result and print congratulations if answer is correct
		if (userResult == (num1 - num2))
			cout << "Your answer is correct!";

		//print correct answer
		else
			cout << "Your answer is not correct! Correct answer is " << num1 - num2 << ". Please try again. " << endl;
	}

	//when user entered incorrect value
	else
		cout << "The value you have entered is incorrect! You had to choose between 'a' and 'b'. Please try again!" << endl;

	return 0;
}


