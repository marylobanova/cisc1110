//Maria Lobanova, Laboratory 11

#include <iostream>
#include <iomanip>
#include <ctime>

using namespace std;

const int SIZE = 5;

// Function prototypes
void pickwinning(int[], int SIZE);
int compareValue(const int[], const int[], int SIZE);

int main()
{
    int winningDigits[5],
        pickfive[5],
        number,
        result;

    //generate five random winning numbers and save to array winningDigits
    pickwinning(winningDigits, SIZE);

    //get five numbers from the user and save to array pickfive
    cout << "Hello! To participate in the lottery you will have to enter five (5) ";
    cout << "random numbers between 1 and 65. " << endl << "----------------------" << endl;
    for (int index = 0; index < 5; index++)
    {
            cout << "Please enter " << index + 1 << " number: ";
            cin >> number;

            //check entered value to be in the range between 1 and 65
            while ((number < 1) || (number > 65))
            {
                cout << "This number should be in the range between 1 and 65, please try again: ";
                cin >> number;
            }
            pickfive[index] = number;
    }

    //compare winning numbers with numbers recieved from the user
    result = compareValue(winningDigits, pickfive, SIZE);
    cout << "----------------------\n----------------------" << endl;
    //if user won

    if (result == SIZE)
        cout << "Congratulations! You won! Winning numbers are: ";

    //if user didn't win
    else
        cout << "Sorry, you didn't win today :( \nWinning numbers are: ";

    //show winning numbers
    for (int index = 0; index < SIZE; index++)
        cout << winningDigits[index] << " ";

    //show numbers provided by user
    cout << "\nYour numbers are: ";

    for (int index = 0; index < SIZE; index++)
        cout << pickfive[index] << " ";

    //acknowledge of number of matches
    cout << "\nNumber of matches is " << result << " out of " << SIZE << endl;

    return 0;
}

//generates random numbers in the range from 1 to 65 and assigns to array winningDigits
void pickwinning(int winningDigits[5], int SIZE)
{
    unsigned seed = time(0);
    srand(seed);
    for (int index = 0; index < SIZE; index++)
        winningDigits[index]= rand() % 65 + 1;
}

//compares generated random numbers with numbers that were received from user
//and returns number of matches
int compareValue(const int winningDigits[5], const int pickfive[5], int SIZE)
{
    int match = 0;
    for (int index = 0; index < SIZE; index++)
        if (winningDigits[index] == pickfive[index])
            match++;

return match;
}

