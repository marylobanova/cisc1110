//Maria Lobanova, Laboratory 9

#include <iostream>
#include <iomanip>
#include "PopInfo.h"

using namespace std;

// Function prototype

int main()
{
	PopInfo cityInfo;
	string 	city;
	int 	population,
			births,
			deaths;

	cout << "What is the name of city?" << endl;
	getline (cin, city);
	cityInfo.setCity(city);

	cout << "What is the population of " << city << "?" << endl;
	cin >> population;
	cityInfo.setPopulation(population);
	while (cityInfo.getPopulation() != population)
		{
			cout << "Population number can not be smaller than 2. Please enter correct number" << endl;
			cin >> population;
			cityInfo.setPopulation(population);
		}


	cout << "What is the birth rate in " << city << "?" << endl;
	cin >> births;
	cityInfo.setBirths(births);
	while (cityInfo.getBirths() != births)
		{
			cout << "Birth rate can not be smaller than 1. Please enter correct number" << endl;
			cin >> births;
			cityInfo.setBirths(births);
		}

	cout << "What is the death rate in " << city << "?" << endl;
	cin >> deaths;
	cityInfo.setDeaths(deaths);
	while (cityInfo.getDeaths() != deaths)
		{
			cout << "Death rate can not be smaller than 1. Please enter correct number" << endl;
			cin >> deaths;
			cityInfo.setDeaths(deaths);
		}

	cout << "\n--------------\n";
	cout << setw(20) << left << "City: " << setw(15) << left << cityInfo.getCity() << endl;
	cout << setw(20) << left << "City population: " << setw(15) << left << cityInfo.getPopulation() << endl;
	cout << setw(20) << left << "Number of births: " << setw(15) << left << cityInfo.getBirths() << endl;
	cout << setw(20) << left << "Number of deaths: " << setw(15) << left << cityInfo.getDeaths() << endl;
	cout << setw(20) << left << "Birth rate: " << setw(15) << left << setprecision(3) << fixed  << cityInfo.getBirthRate() << "%" << endl;
	cout << setw(20) << left << "Deaths rate: " << setw(15) << left << setprecision(3) << fixed << cityInfo.getDeathRate() << "%" << endl;
	cout << "--------------" << endl;

    return 0;
}


