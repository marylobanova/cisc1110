//Maria Lobanova, Laboratory 2

#include <iostream>

using namespace std;

int main ()
{
	int shares;
	double buyPrice, purchaseTotal, sellPrice, saleTotal;

	//get number of shares that person wants to buy
	cout << "Please enter number of stock shares that you want to buy: ";
	cin >> shares;

	//get the purchase price per one share
	cout << "Please enter purchase price per share: ";
	cin >> buyPrice;

	//caclulate total amount to pay
	purchaseTotal = shares * buyPrice;

	//get the selling price per one share
	cout << "Please enter selling price per share: ";
	cin >> sellPrice;

	//caclulate total amount from sale
	saleTotal = shares * sellPrice;

	cout << "\nTotal number of shares purchased is " << shares << endl;
	cout << "Total price paid is $" << purchaseTotal << endl;
	cout << "Total amount received is $" << saleTotal << endl;

	return 0;

	}

	//output
	
	(MARIA@Marias-MacBook-Pro)(0) ~ (● HEAD) 
	✣ /Users/MARIA/Desktop/CISC\ 1110\ \(C++\)/Laboratory-2 ; exit;
	Please enter number of stock shares that you want to buy: 125
	Please enter purchase price per share: 12.45
	Please enter selling price per share: 14.98

	Total number of shares purchased is 125
	Total price paid is $1556.25
	Total amount received is $1872.5
	logout

	[Process completed]
