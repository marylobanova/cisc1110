//Maria Lobanova, Laboratory 6

#include <iostream>
#include <iomanip>

using namespace std;

int main()
{
	double 	minutes,
			conversationCost;
	char 	selection,
			answer;

    cout << "Hello! ";
	do
	{
		//ask user how many minutes they want to talk
		cout << "How many minutes do you want to talk? ";
		cin >> minutes;

		//validate input to be positive only
		while (minutes <= 0)
		{
			cout << "The number you entered is incorrect. Please enter a positive number: ";
			cin >> minutes;
		}

		//ask user to select the provider
		cout << "Please choose the provider from the list below (choose a, b, c or d)" << endl;
		cout << "a) Vonage World, $0.34 / min" << endl;
		cout << "b) Skype pay-as-you-go, $0.10 / min" << endl;
		cout << "c) Google Voice, $0.30 / min" << endl;
		cout << "d) T-mobile pay-as-you-go, $1.99 / min" << endl;
		cin >> selection;

        //calculate rate for each selection
		switch(selection)
		{
			case 'A':
			case 'a': conversationCost = minutes*0.34;
					cout << setw(20) << left << "----------" << endl
					<< "Total cost of conversation: $" << fixed << setprecision(2) << conversationCost << endl
					<< "Selected provider: Vonage World" << endl
					<< "Number of minutes: " << minutes << endl
					<< setw(20) << left << "----------" << endl;
					break;

			case 'B':
			case 'b': conversationCost = minutes*0.10;
					cout << setw(20) << left << "----------" << endl
					<< "Total cost of conversation: $" << fixed << setprecision(2) << conversationCost << endl
					<< "Selected provider: Skype" << endl
					<< "Number of minutes: " << minutes << endl
					<< setw(20) << left << "----------" << endl;
					break;

			case 'C':
			case 'c': conversationCost = minutes*0.30;
					cout << setw(20) << left << "----------" << endl
					<< "Total cost of conversation: $" << fixed << setprecision(2) << conversationCost << endl
					<< "Selected provider: Google Voice" << endl
					<< "Number of minutes: " << minutes << endl
					<< setw(20) << left << "----------" << endl;
					break;

			case 'D':
			case 'd': conversationCost = minutes*1.99;
					cout << setw(20) << left << "----------" << endl
					<< "Total cost of conversation: $" << fixed << setprecision(2) << conversationCost << endl
					<< "Selected provider: T-mobile pay-as-you-go" << endl
					<< "Number of minutes: " << minutes << endl
					<< setw(20) << left << "----------" << endl;
					break;

			default: 	cout << "You entered an incorrect value. Please try again. ";
						break;
		}

		cout << "\nDo you want to continue? (Y/N) ";
		cin >> answer;
	}
	while ((answer == 'Y') || (answer == 'y'));

	return 0;
}


