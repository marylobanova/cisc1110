//Maria Lobanova, Laboratory 2

#include <iostream>

using namespace std;

int main ()
{
	int shares;
	double buyPrice, purchaseTotal, sellPrice, saleTotal;

	//get number of shares that person wants to buy
	cout << "Please enter number of stock shares that you want to buy: ";
	cin >> shares;

	//get the purchase price per one share
	cout << "Please enter purchase price per share: ";
	cin >> buyPrice;

	//caclulate total amount to pay
	purchaseTotal = shares * buyPrice;

	//get the selling price per one share
	cout << "Please enter selling price per share: ";
	cin >> sellPrice;

	//caclulate total amount from sale
	saleTotal = shares * sellPrice;

	cout << "\nTotal number of shares purchased is " << shares << endl;
	cout << "Total price paid is $" << purchaseTotal << endl;
	cout << "Total amount received is $" << saleTotal << endl;

	return 0;

	}
