#include <iostream>
#include <ctype.h>
#include <iomanip>

/* Macro that calculates a size of any type of array */
#define ARRAY_SIZE(array) (sizeof((array))/sizeof((array[0])))

using namespace std;

/* 
  In general having multiple long strings mixed with code is not good idea. 
  The good practice is to keep it in separated header files (but for toy 
  code it's fine to keep it in single file).
*/
const string 
MSG_HELLO = 
  "Hello! How many minutes do you want to talk?",
MSG_INCORRECT_NUMBER =  
  "The number you entered is incorrect. Please enter a positive number: ",
MSG_INCORRECT_SELECTION =
  "You entered an incorrect value. Please try again. ",
MSG_SELECTION = 
  "Please choose the provider from the list below (choose a, b, c or d)",
MSG_CONTINUE = 
  "Do you want to continue? (Y/N)";

/*
  Provider is the best candidate for a class/struct in given exercise. 
  [WAYOFTHINKING]: 
  read the problem ; search for entities ; select entities that are good 
  candidates for classes (not primitives)
*/
struct Provider {
    char id;
    double minuteRate;
    string name;
  public:
    double conversationCost(int);
    void summary(int);

};

double Provider::conversationCost(int minutes) {
  return minuteRate*minutes;
}

void Provider::summary(int minutes) { 
  cout  << left << "----------\n"
        << "Total cost of conversation: " << this->conversationCost(minutes) << endl
        << "Selected provider: " << name << endl 
        << "Number of minutes: " << minutes << endl
        << setw(20) << left << "----------\n";
};
/* Overload << operator allows to cout instances of Provider class */ 
std::ostream &operator<<(std::ostream &os, Provider const &p) { 
    return os << p.id << ") " << p.name << ", $" << p.minuteRate << " / min";
};

/* Define your list of providers */
Provider providers[] = {
  {'a', 0.34, "Vonage World"},
  {'b', 0.10, "Skype pay-as-you-go"},
  {'c', 0.30, "Google Voice"},
  {'d', 1.99, "T-mobile pay-as-you-go"}
};
Provider findProviderById(char id) {
  for(int i=0 ; i < ARRAY_SIZE(providers); ++i) 
    if(providers[i].id == id) return providers[i];
}

int main()
{
  double minutes, conversationCost;
  char selection, answer;

  do {
    //ask user how many minutes they want to talk
    cout << MSG_HELLO << endl;
    cin >> minutes;

    //validate input to be positive only
    while (minutes <= 0) {
      cout << MSG_INCORRECT_NUMBER;
      cin >> minutes;
    }
    //ask user to select the provider
    cout << MSG_SELECTION << endl;
    for(int i=0 ; i < ARRAY_SIZE(providers); ++i) 
      cout << providers[i] << endl;
    cin >> selection;

    //calculate rate for each selection
    switch(tolower(selection))
    {
      case 'a': case 'b': case 'c': case 'd':
        findProviderById(tolower(selection)).summary(minutes);
        break;
      default:  
        cout << MSG_INCORRECT_SELECTION << endl;
        break;
    }

    cout << endl << MSG_CONTINUE << endl;
    cin >> answer;
  } while (tolower(answer) == 'y');

  return 0;
}

