//Maria Lobanova, Laboratory 4-1

# include <iostream>
# include <iomanip>
# include <cmath>
# include <string>

using namespace std;

int main()
{
    string 	month1,
    		month2,
    		month3;

    double	highTempMonth1,
    		highTempMonth2,
    		highTempMonth3,
    		lowTempMonth1,
    		lowTempMonth2,
    		lowTempMonth3,
    		rainfallMonth1,
    		rainfallMonth2,
    		rainfallMonth3,
    		avgTempMonth1,
    		avgTempMonth2,
    		avgTempMonth3,
    		avgRainfall;


	//get information about month 1
	cout << "What is the name of the first month? ";
	cin >> month1;
	cout << "What is the highest temperature of " << month1 << "? ";
	cin >> highTempMonth1;
	cout << "What is the lowest temperature of " << month1 << "? ";
	cin >> lowTempMonth1;
	cout << "What is the total rainfall for " << month1 << "? ";
	cin >> rainfallMonth1;
	cout << "--------------";

	//calculate average temperature for month 1
	avgTempMonth1 = (highTempMonth1 + lowTempMonth1)/2;

	//get information about month 2 and calculate average temperature for it
	cout << "\nWhat is the name of the second month? ";
	cin >> month2;
	cout << "What is the highest temperature of " << month2 << "? ";
	cin >> highTempMonth2;
	cout << "What is the lowest temperature of " << month2 << "? ";
	cin >> lowTempMonth2;
	cout << "What is the total rainfall for " << month2 << "? ";
	cin >> rainfallMonth2;
	cout << "--------------";

	//calculate average temperature for month 2
	avgTempMonth2 = (highTempMonth2 + lowTempMonth2)/2;

	//get information about month 3 and calculate average temperature for it
	cout << "\nWhat is the name of the third month? ";
	cin >> month3;
	cout << "What is the highest temperature of " << month3 << "? ";
	cin >> highTempMonth3;
	cout << "What is the lowest temperature of " << month3 << "? ";
	cin >> lowTempMonth3;
	cout << "What is the total rainfall for " << month3 << "? ";
	cin >> rainfallMonth3;
	cout << "--------------\n";

	//calculate average temperature for month 3
	avgTempMonth3 = (highTempMonth3 + lowTempMonth3)/2;

	//calculate average rainfall for three months
	avgRainfall = (rainfallMonth1 + rainfallMonth2 + rainfallMonth3)/3;

	//output data for all months
	cout << "\nWeather Statistics\n";
	cout << "--------------\n";
	cout << fixed << setprecision(1);
	cout << left << setw(25) << "Data / Month Name" << left << setw(15) << month1 << setw(15) << month2 << setw(15) << month3 << endl;
	cout << left << setw(25) << "Highest Temperature" << left <<  setw(15) << highTempMonth1 << setw(15) << highTempMonth2 << setw(15) << highTempMonth3 << endl;
	cout << left << setw(25) << "Lowest Temperature" << left <<  setw(15) << lowTempMonth1 << setw(15) << lowTempMonth2 << setw(15) << lowTempMonth3 << endl;
	cout << left << setw(25) << "Average Temperature" << left <<  setw(15) << avgTempMonth1 << setw(15) << avgTempMonth2 << setw(15) << avgTempMonth3 << endl;
	cout << left << setw(25) << "Total Rainfall" << left << setw(15) << highTempMonth1 << setw(15) << highTempMonth2 << setw(15) << highTempMonth3 << endl;

	//output average rainfall
	cout << "\nAverage rainfall for " << month1 << ", " << month2 << " and " << month3 << " is " << avgRainfall << endl;

	return 0;
}



