//Maria Lobanova, Laboratory 9
#ifndef POPINFO_H
#define POPINFO_H
#include <string>

//popinfo class declaration
class PopInfo
{
private:
	std::string cityName;
	long        cityPopulation;
	int         numBirths,
                numDeaths;

public:
	//default constructor
	PopInfo()
	{
		cityName = "";
		cityPopulation = 2;
		numDeaths = 1;
		numBirths = 1;
	}

    void setPopulation (long population);
	void setBirths (int);
	void setDeaths (int);
	void setCity (std::string city);

	long getPopulation ();
	int getBirths ();
	int getDeaths ();
	std::string getCity ();

	double getBirthRate ();
	double getDeathRate ();

};

//get city name
void PopInfo::setCity (std::string city)
 {
 	cityName = city;
 }

//get population size for selected city
void PopInfo::setPopulation (long population)
{
	if (population > 1)
	cityPopulation = population;
}

//get number of births for selected city
void PopInfo::setBirths (int births)
{
	if (births > 0)
	numBirths = births;
}

//get number of deaths for selected city
void PopInfo::setDeaths (int deaths)
{
	if (deaths > 0)
	numDeaths = deaths;
}

std::string PopInfo::getCity ()
 {
 	return cityName;
 }

long PopInfo::getPopulation ()
{
	return cityPopulation;
}

int PopInfo::getBirths ()
{
	return numBirths;
}

int PopInfo::getDeaths ()
{
	return numDeaths;
}

double PopInfo::getBirthRate ()
{
	double birthRate = static_cast<double>(numBirths)/cityPopulation;
	return birthRate;
}

double PopInfo::getDeathRate ()
{
	double deathRate = static_cast<double>(numDeaths)/cityPopulation;
	return deathRate;
}

#endif
