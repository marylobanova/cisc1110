//Maria Lobanova, Laboratory #1

#include <iostream>
#include <math.h>

using namespace std;

const double taxRate = 0.0875;

int main()

{
    double amount, taxAmount;
    amount = 101.85;
    taxAmount = ceil(amount*taxRate*100)/100;

    //display the tax amount
    cout << "For the amount of $" << amount << " tax rate will be $" << taxAmount << endl;

    return 0;

}
