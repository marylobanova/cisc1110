//Maria Lobanova, Laboratory 3-2

# include <iostream>
# include <iomanip>
# include <cmath>

using namespace std;

int main()
{
    double principal, interestRate, interestFrequency, interest, finalBalance;

	//get the principal
	cout << "How much money do you have in the account? ";
	cin >> principal;

	//get the interest rate
	cout << "What is the interest rate (in percent %)? ";
	cin >> interestRate;

	//get number of times that interest is calculated
	cout << "How many times the interest will be calculated? ";
	cin >> interestFrequency;

	//calculate the final balance
	finalBalance = pow((1.0 + (interestRate/100)/interestFrequency),interestFrequency)*principal;

    //calculate the interest
	interest = finalBalance - principal;

    //provide final balance amount
	cout    << fixed << setprecision(2)
            << "-------\nFor the principal of $" << principal
            << ", interest rate of " << interestRate
            << "% and " << static_cast<int>(interestFrequency)
            << " times the interest is calculated, the interest earned will be $" << interest
            << " and the total balance will be $" << finalBalance << endl;

}

