//Maria Lobanova, Laboratory 7

#include <iostream>
#include <iomanip>

using namespace std;

// Function prototype
double celcius(double F);

int main()
{
	double 	F,
			C;

	//set table headers
	cout << setw(12) << left << "Farenheit" << setw(12) << left << "Celcius" << endl;

	//print Fahrenheit and Celcius values from 20 to 40 degrees Fahrenheit
	for (F = 20.00; F < 41; F++)
	{
		C = celcius(F);
		cout << setprecision(2) << fixed << setw(12) << left << F << setw(12) << left << C << endl;
	}

    return 0;
}

//calculate Celcius from Fahrenheit
double celcius(double F)
{
	return (F - 32)/(9.0/5);
}


//C = (F - 32)/(9.0/5);
