//Maria Lobanova, Laboratory 8

#include <iostream>
#include <iomanip>

using namespace std;

// Function prototype
bool isPrime(int number);

int main()
{
    int number,
        primeNumCount = 0;

    //evalute each ordinal number starting from 2 until 20 prime numbers are determined
    for(number = 2; primeNumCount < 21; number++)
    {
       if (isPrime(number) == true)
       {
            primeNumCount++;
            cout << setw(3) << left << number << " is Prime" << endl;
       }

        else
            cout << setw(3) << left << number << " is Complex" << endl;
    }

    return 0;
}

bool isPrime(int number)
{
    //divide provided number by all values between 1 and its' half
    for (int denominator = 2; denominator < (number/2 + 1); denominator++)
    {
        int divisionRemainder = number%denominator;

        //if remainder is equal to zero at least once, provided number is not prime
        if (divisionRemainder == 0)
            return false;

    }
		return true;

}
