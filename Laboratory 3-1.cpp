//Maria Lobanova, Laboratory 3-1

# include <iostream>
# include <cmath>

using namespace std;

int main()
{	double F,C;

	//get the temperature in Celcius
	cout << "What is the temperature in Celcius? ";
	cin >> C;

	//calculate the temperature in Farenheit
	F = (9.0/5) * C + 32;

    //provide temperature in Farenheit
	cout << C << " degrees in Celcius are equal to " << F << " degrees in Farenheit." << endl;

}

